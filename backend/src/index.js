// const debug = require('debug')('weathermap');

const Koa = require('koa');
const router = require('koa-router')();
const fetch = require('node-fetch');
const cors = require('kcors');

const appId = process.env.APPID || '';
const mapURI = process.env.MAP_ENDPOINT || 'http://api.openweathermap.org/data/2.5';
const targetCity = process.env.TARGET_CITY || 'Helsinki,fi';

const port = process.env.PORT || 9000;

const app = new Koa();

app.use(cors());

const fetchWeather = async () => {
  const endpoint = `${mapURI}/weather?q=${targetCity}&appid=${appId}&`;
  const response = await fetch(endpoint);

  return response ? response.json() : {};
};

const fetchForecastWeather = async () => {
  const endpoint = `${mapURI}/forecast?q=${targetCity}&appid=${appId}&`;
  const response = await fetch(endpoint);

  return response ? response.json() : {};
};

const fetchCurrentLocationWeather = async (lat, lon) => {
  const endpoint = `${mapURI}/weather?lat=${lat}&lon=${lon}&appid=${appId}`;
  const response = await fetch(endpoint);

  return response ? response.json() : {};
};

const fetchCurrentLocationForecast = async (lat, lon) => {
  const endpoint = `${mapURI}/forecast?lat=${lat}&lon=${lon}&appid=${appId}`;
  const response = await fetch(endpoint);

  return response ? response.json() : {};
};

router.get('/health', ctx => {
  ctx.body = 'Its working fine';
});

router.get('/api/weather', async ctx => {
  const weatherData = await fetchWeather();

  ctx.type = 'application/json; charset=utf-8';
  ctx.body = weatherData;
});

router.get('/api/forecastweather', async ctx => {
  const weatherData = await fetchForecastWeather();

  ctx.type = 'application/json; charset=utf-8';
  ctx.body = weatherData;
});

router.get('/api/current', async ctx => {
  if (!ctx.query || !ctx.query.lat || !ctx.query.lon) {
    ctx.status = 400;
    ctx.message = 'Bad Query String';
  } else {
    const weatherData = await fetchCurrentLocationWeather(ctx.query.lat, ctx.query.lon);
    ctx.type = 'application/json; charset=utf-8';
    ctx.body = weatherData;
  }
});

router.get('/api/currentforecast', async ctx => {
  if (!ctx.query || !ctx.query.lat || !ctx.query.lon) {
    ctx.status = 400;
    ctx.message = 'Bad Query String';
  } else {
    const weatherData = await fetchCurrentLocationForecast(ctx.query.lat, ctx.query.lon);
    ctx.type = 'application/json; charset=utf-8';
    ctx.body = weatherData;
  }
});

app.use(router.routes());
app.use(router.allowedMethods());

app.listen(port);

console.log(`App listening on port ${port}`);

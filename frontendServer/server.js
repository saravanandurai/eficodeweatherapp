var express = require('express'),
	bodyParser = require('body-parser');
var	app = express();

var PORT = 8083;

var hostname = "139.59.34.115";

// EXPRESS CONFIG
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + '/dist/'));

// ROUTES
app.get('/', function(req, res){    
	res.sendFile(__dirname +  '/dist/index.html');
});

// Start server
app.listen(PORT, hostname,function(){
  console.log('Server listening on port ' + PORT)
});
const path = require('path');
const webpack = require('webpack');

const TransferWebpackPlugin = require('transfer-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  context: __dirname,
  entry: './src/index.js', 
  devtool: 'eval',
  devServer: {
    host: '0.0.0.0',     
    port: 8083,
    stats: 'verbose'
  },
  output: {
    path: path.join(__dirname, '/dist'),
    filename: 'index.bundle.js'    
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [{
          loader: 'babel-loader'          
        }],
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: 'style-loader'
          },
          {
              loader: 'css-loader',
              options: {
                modules: true,
                importLoaders: 1,
                localIdentName: '[name]_[local]_[hash:base64]',
                sourceMap: true,
                minimize: true
              }
          },
          {
            loader: 'sass-loader'
          }            
        ]

      },
      {
        test: /\.css/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader',
            options: {
              modules: true,
              importLoaders: 1,
              localIdentName: '[name]_[local]_[hash:base64]',
              sourceMap: true,
              minimize: true
            }
          }
      ]
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              outputPath: 'assets/'
            }
          }
        ]
      }

    ],
  },
  plugins: [
    new HtmlWebpackPlugin({ 
      template: './src/index.html',      
    }),        
    new webpack.DefinePlugin({
      'process.env': {
        ENDPOINT: JSON.stringify(process.env.ENDPOINT || 'http://139.59.34.115:9000/api'),
      },
    }),
    new CopyWebpackPlugin([
      {from: './src/assets' , to: 'assets'}
    ])
  ],
  /*
  watchOptions: {
    aggregateTimeout: 300,
    poll: 1000,
  }
  */
};

import App from './app';
import {mount, shallow} from 'enzyme';
import { expect } from 'chai';
import waitUntil from 'async-wait-until';

import {weather, forecast} from './data'; 


const responseWeather = {
    json: () => {
        return new Promise((resolve) => {            
            resolve(weather)
        })
    }
};

const responseForecast = {
    json: () => {
        return new Promise((resolve) => {            
            resolve(forecast)
        })
    }
};

describe('<App />',  () => {
    before(() => {
        global.fetch = (arg) => {               
            let argFormat = String(arg).split('/')[1]
            let argFinal = argFormat.split('?')[0]                    
            if(argFinal === 'weather' || argFinal  === 'current'){
                return new Promise(resolve => {
                    resolve(responseWeather);
                })
            } else if(argFinal === 'forecastweather' || argFinal === 'currentforecast'){
                return new Promise(resolve => {
                    resolve(responseForecast);
                })
            }
            return ''            
        }
    })
    
    it('helsinki state is not null and has values', async () => {        
        let wrapper = shallow(<App />)        
        await waitUntil(() => {                        
            return wrapper.state('helsinki') !== '' ;
        })    
        expect(wrapper.state('helsinki')).to.be.not.equal('');
        expect(wrapper.state('helsinki')).to.have.nested.property('coord.lon');
        expect(wrapper.state('helsinki')).to.have.nested.property('coord.lat');
        expect(wrapper.state('helsinki')).to.have.nested.property('weather[0].icon');
        expect(wrapper.state('helsinki').weather[0]).to.include.all.keys('main','description');
        wrapper.unmount();
    })
    
    it('current state is not null and has values', async () => {        
        let wrapper = shallow(<App />)        
        await waitUntil(() => {                        
            return wrapper.state('current') !== '' ;
        })
            
        expect(wrapper.state('current')).to.be.not.equal('', 'State:' + JSON.stringify(wrapper.state()));
        expect(wrapper.state('current')).to.have.property('coord');
        expect(wrapper.state('current')).to.have.property('weather');
        wrapper.unmount()
    })

    it('Helsinki forecast is not empty and has list property with len 40, has properties city, name, coord', async () => {
        let wrapper = shallow(<App />)
        await waitUntil(() => {
            return wrapper.state('helsinkiForecast') !== '';
        })
        expect(wrapper.state('helsinkiForecast')).to.be.not.equals('');
        expect(wrapper.state('helsinkiForecast')).to.have.property('list').of.length(40);
        expect(wrapper.state('helsinkiForecast')).to.have.nested.property('list[0].main.temp');
        expect(wrapper.state('helsinkiForecast')).to.have.nested.property('list[0].dt');
        expect(wrapper.state('helsinkiForecast')).to.have.nested.property('list[0].weather[0].icon');
             
        wrapper.unmount();
    })

    it('Have two card Components',  async ()=>{
        let wrapper = shallow(<App />);
        await waitUntil(()=> {
            return  wrapper.state('helsinki') !== '' && wrapper.state('current') !== '' && 
                    wrapper.state('helsinkiForecast') !== '' && wrapper.state('currentForecast') !== '';
        })
               
        expect(wrapper.find('Card')).to.have.length(2);                        
        wrapper.unmount();
    })

})







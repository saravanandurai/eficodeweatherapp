import 'babel-polyfill';

import React from 'react';

import styles from './app.scss';
import {Card} from '../card/card';

const baseURL = process.env.ENDPOINT;

const getWeatherFromApi = async () => {
    try {
      const response = await fetch(`${baseURL}/weather`);
      return response.json();
    } catch (error) {
      console.error(error);
    }
  
    return {};
};

const getWeatherForecastFromApi = async () => {
    try {
      const response = await fetch(`${baseURL}/forecastweather`);
      return response.json();
    } catch (error) {
      console.error(error);
    }
  
    return {};
};

const getCurrentLocationWeatherFromApi = async () => {
    try {        
        const position = await getCurrentLocation()  
        const response = await fetch(`${baseURL}/current?lat=${position.coords.latitude}&lon=${position.coords.longitude}`)
        return response.json()            
                      
    } catch (error) {
      console.error(error)
    }      
};

const getCurrentLocationForecastFromApi = async () => {
    try {
        const position = await getCurrentLocation()                 
        const response = await fetch(`${baseURL}/currentforecast?lat=${position.coords.latitude}&lon=${position.coords.longitude}`)
        return response.json()            
                      
    } catch (error) {
      console.error(error)
    }      
};

const getCurrentLocation = () => {
    return new Promise((resolve,reject) => {        
        navigator.geolocation.getCurrentPosition(resolve, reject)
    })
};

export default class App extends React.Component {
    constructor(){
        super()
        this.state = {
            helsinki: '',
            helsinkiForecast:'',
            current: '',
            currentForecast:'',
        }

    }

    async componentWillMount(){              
        const helsinki = await getWeatherFromApi();                      
        const current = await getCurrentLocationWeatherFromApi();       
        const helsinkiForecast = await getWeatherForecastFromApi();
        const currentForecast = await getCurrentLocationForecastFromApi();        
        this.setState({helsinki, current, helsinkiForecast, currentForecast});          
    }

    render(){        
        if(!this.state.helsinki || !this.state.helsinkiForecast || !this.state.current || !this.state.currentForecast){
            return (
                <div>
                    <h2>Loading . . . </h2>
                </div>
            )
        }
        
        return (
            <div className={styles.wrap}>
                <h1 className={styles.headerClass}>Weather App</h1>
                <div className={styles.bodyClass}>
                    <div className={styles.cardWrap}>
                        {this.state.helsinki && this.state.helsinkiForecast &&
                        <Card data={this.state.helsinki} forecast={this.state.helsinkiForecast} style={{'marginRight': '1rem'}}/>}
                        {this.state.current && this.state.currentForecast &&
                        <Card data={this.state.current} forecast={this.state.currentForecast} style={{'marginLeft': '1rem'}}/>}
                    </div>
                    
                </div>
            </div>
        )
    }
}

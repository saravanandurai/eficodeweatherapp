import React from 'react';

import styles from './card.scss';

import moment from 'moment'; 



const getCelcius = (kelvin) => {    
    return Math.round((kelvin - 273.15)*100)/100 
}

const getFarenheit = (kelvin) => {
    return Math.round(((kelvin * 9/5) - 459.67) *100)/100
} 

const speedConverter = (speed) => {
    return Math.round(((speed * 60 * 60) / 1000) * 100)/100
}

const forecastComponent = (data, index) =>{    
    const icon = data.weather[0].icon.slice(0,2)
    return (
        <div key={data.dt}>
            <h4>{moment().add(3*(index + 1), 'h').format('HH:mm')} hrs</h4>
            { icon && <img src={`assets/img/${icon}.svg`} className={styles.forecastIcon} /> }
            <h5>{getCelcius(data.main.temp)} &#176; C <br/>{data.main.humidity} %</h5>
        </div>
    )
}

export const Card  = (props) => {
    const data = props.data     
    const sunrise = moment.unix(data.sys.sunrise).toString() || null
    const sunset = moment.unix(data.sys.sunset).toString() || null
    
    const forecastArr = props.forecast.list.slice(0,6) 
    const icon = data.weather[0].icon.slice(0,2)
    
    return(

        <div className={styles.cardWrap} style={props.style}>            
            
            <h3>{data.name}, {data.sys.country}
                <span className={styles.coords}>Lat: {data.coord.lat} / Lon: {data.coord.lon}</span>
            </h3>             
            <h4>Temp: {getCelcius(data.main.temp)} &#176; C / {getFarenheit(data.main.temp)} &#176; F </h4>
            <h4>Range: {getCelcius(data.main.temp_min)} -  {getCelcius(data.main.temp_max)} &#176; C / {getFarenheit(data.main.temp_min)} -  {getFarenheit(data.main.temp_max)} &#176; F </h4>
            <h4>{data.weather[0].main} - {data.weather[0].description} 
                <img src={`assets/img/${icon}.svg`} alt="" className={styles.mainIcon}/> </h4>
            <h4>Humidity: {data.main.humidity} %</h4>
            <h4>Wind: {speedConverter(data.wind.speed)} kmph</h4>
            <h4>Sunrise: {sunrise}</h4>
            <h4>Sunset: {sunset}</h4>
            <h4>Visibility: {data.visibility} m</h4>
            <h4>Forecast</h4>                
            <div className={styles.forecastWrap}>
                {forecastArr.map((item, index)=>{
                    return forecastComponent(item, index)
                })}
            </div>

        </div>
    )
}


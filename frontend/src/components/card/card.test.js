import {mount} from 'enzyme';
import { expect } from 'chai';

import {weather, forecast} from '../app/data'; 
import {Card} from './card';

describe('<Card />' , () => {
    let wrapper = null;
    before(() => {
        wrapper = mount(<Card data={weather} forecast={forecast}/>);        
    })

    it('Have a h3 element with value Helsinki, FI Lat: 60.17 / Lon: 24.94', () => {        
        expect(wrapper.find('h3')).to.have.length(1);
        expect(wrapper.find('h3').text()).to.contain('Helsinki, FI');        
        expect(wrapper.find('h3').text()).to.contain('Lat: 60.17 / Lon: 24.94');
    })

    it('Have a h4 element occuring 9 + 6 times', () => {        
        expect(wrapper.find('h4')).to.have.length(15);        
    })

    it('Have Text: Range: 11 -  11 ° C / 51.8 -  51.8 ° F', () => {        
        expect(wrapper.text()).to.contain('Range: 11 -  11 ° C / 51.8 -  51.8 ° F');                
    })

    it('Has a 1 wrapping div + 6 inner divs in forecast', () => {           
        expect(wrapper.find('div').at(1).find('div').first().find('div')).to.have.length(7)        
    })    

    after(() => {
        wrapper.unmount();
    })



})